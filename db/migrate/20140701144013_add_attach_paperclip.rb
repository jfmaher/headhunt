class AddAttachPaperclip < ActiveRecord::Migration
  def self.up
    add_attachment :candidates, :cv
  end

  def self.down
    remove_attachment :candidates, :cv
  end
end
