class CreateSalaryTypes < ActiveRecord::Migration
  def change
    create_table :salary_types do |t|
      t.string :name
      t.string :description
      t.string :currency

      t.timestamps
    end
  end
end
