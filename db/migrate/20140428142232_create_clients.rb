class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :fist_name
      t.string :last_name
      t.string :phome
      t.string :location
      t.integer :user_id
      t.integer :company_id
      t.integer :position_id

      t.timestamps
    end
  end
end
