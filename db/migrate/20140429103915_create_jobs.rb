class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :job_title
      t.integer :salary_id
      t.string :location
      t.integer :job_type_id
      t.integer :subcategory_id
      t.integer :client_id
      t.date :start_date
      t.date :expire_date
      t.text :content
      t.integer :education_level_id
      t.integer :career_level_id
      t.string :contact_details

      t.timestamps
    end
  end
end
