class ChangePhomeInClients < ActiveRecord::Migration
  def change
    rename_column(:clients, :phome, :phone)
  end
end
