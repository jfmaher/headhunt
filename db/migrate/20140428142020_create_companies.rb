class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.text :addressHQ
      t.string :phone
      t.string :logopath
      t.string :owner
      t.string :website
      t.string :supportemail
      t.string :googlemapURL

      t.timestamps
    end
  end
end
