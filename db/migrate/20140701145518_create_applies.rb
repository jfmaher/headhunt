class CreateApplies < ActiveRecord::Migration
  def change
    create_table :applies do |t|
      t.integer :candidate_id
      t.integer :job_id
      t.boolean :active
      t.boolean :reject
      t.string :comment

      t.timestamps
    end
  end
end
