class AddServiceLevelToClient < ActiveRecord::Migration
  def change
    add_column :clients, :service_level_id, :integer
  end
end
