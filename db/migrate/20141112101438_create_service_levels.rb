class CreateServiceLevels < ActiveRecord::Migration
  def change
    create_table :service_levels do |t|
      t.string :name
      t.integer :job_limit

      t.timestamps
    end
  end
end
