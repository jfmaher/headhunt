class CreateKnowledges < ActiveRecord::Migration
  def change
    create_table :knowledges do |t|
      t.integer :skill_id
      t.integer :candidate_id
      t.float :time

      t.timestamps
    end
  end
end
