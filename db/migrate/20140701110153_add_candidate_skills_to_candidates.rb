class AddCandidateSkillsToCandidates < ActiveRecord::Migration
  def change
    add_column :candidates, :candidate_skills, :string
  end
end
