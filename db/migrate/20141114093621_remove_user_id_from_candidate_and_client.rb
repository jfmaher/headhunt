class RemoveUserIdFromCandidateAndClient < ActiveRecord::Migration
  def change
    remove_column :candidates, :user_id
    remove_column :clients, :user_id
  end
end
