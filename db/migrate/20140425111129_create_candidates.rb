class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :location
      t.integer :user_id

     

      t.timestamps
    end
  end
end
