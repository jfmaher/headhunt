class ChangeSalaryMaxMinTypes < ActiveRecord::Migration
  def change
    change_column :salaries, :max, :integer
    change_column :salaries, :min, :integer
  end
end
