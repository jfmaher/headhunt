class CreateSalaries < ActiveRecord::Migration
  def change
    create_table :salaries do |t|
      t.float :min
      t.float :max
      t.integer :salary_type_id

      t.timestamps
    end
  end
end
