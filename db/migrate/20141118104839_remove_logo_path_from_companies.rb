class RemoveLogoPathFromCompanies < ActiveRecord::Migration
  def change
    remove_column :companies, :logopath, :string
  end
end
