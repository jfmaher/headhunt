# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
services = [['platinum', 100],
           ['gold', 3],
           ['silver', 1],
           ['free', 0]]
services.each do |name, job_l|
  ServiceLevel.create(:name => name, :job_limit => job_l)
end

cat_list = ["IT",
			"Medicine",
			"Aviation"]
cat_list.each do |cat|
	JobType.create({name: cat})
end

country_list = [["London", "UK"],["Dublin", "Ireland"],["Paris","France"]]

country_list.each do |loc, name|
	Country.create({name: name})
end

salaries_step = 10000
(0..19).each do |i|
  Salary.create(min: i*salaries_step, max: (i+1) * salaries_step)
end

# Create a dummy recruiter and company
@com = Company.create(name: 'newcorp', addressHQ: '25 new street', phone: '2555555', website: 'www.google.com', supportemail: 'j@d.com')
@com.logo = File.open('app/assets/images/blue_target.jpg')
@com.save!
@testClient = Client.create!(first_name: 'j', last_name: 'd', email: 'j@d.com', password: 'testtest', password_confirmation: 'testtest', company_id: @com.id)
@com.client_id = @testClient.id
@com.save!

job_list = ["programmer","Nurse","Pilot"]

job_list.zip(cat_list) do |title, type|
	country_list.each do  |loc, name|
			@typeID = JobType.where(name: type).take
			@countryID = Country.where(name: name).take
            @sal = Salary.where(min: 30000).take
			Job.create({job_title: title, location: loc, job_type_id: @typeID.id, country_id: @countryID.id, content: 'Some Details', client_id: @testClient.id, salary_id: @sal.id})
	end
end
user_list = [["Benny", "Wilkerson", "b@w.com", "testtest"],
             ["Bonnie",	"Edwards", "b@e.com", "testtest"],
             ["Leroy", "Owen", "l@o.com", "testtest"], 
             ["Belinda", "Moss", "b@m.com", "testtest"],
             ["Rolando", "Young", "r@y.com", "testtest"],
             ["Brent", "Robertson", "b@r.com", "testtest"],
             ["Santiago", "Holt", "s@h.com", "testtest"],
             ["Steven", "Wheeler", "s@w.com", "testtest"],
             ["Erma", "Ball", "e@b.com", "testtest"],
             ["Kelly", "Castillo", "k@c.com", "testtest"]]
user_list.each do |f_name, l_name, email, pass|
  Candidate.create!(:first_name => f_name, :last_name => l_name, :email => email, :password => pass, :password_confirmation => pass)
end
