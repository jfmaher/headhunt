require 'test_helper'

class SalaryTypesControllerTest < ActionController::TestCase
  setup do
    @salary_type = salary_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:salary_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create salary_type" do
    assert_difference('SalaryType.count') do
      post :create, salary_type: { currency: @salary_type.currency, description: @salary_type.description, name: @salary_type.name }
    end

    assert_redirected_to salary_type_path(assigns(:salary_type))
  end

  test "should show salary_type" do
    get :show, id: @salary_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @salary_type
    assert_response :success
  end

  test "should update salary_type" do
    patch :update, id: @salary_type, salary_type: { currency: @salary_type.currency, description: @salary_type.description, name: @salary_type.name }
    assert_redirected_to salary_type_path(assigns(:salary_type))
  end

  test "should destroy salary_type" do
    assert_difference('SalaryType.count', -1) do
      delete :destroy, id: @salary_type
    end

    assert_redirected_to salary_types_path
  end
end
