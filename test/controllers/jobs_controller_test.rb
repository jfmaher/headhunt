require 'test_helper'

class JobsControllerTest < ActionController::TestCase
  setup do
    @job = jobs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:jobs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create job" do
    assert_difference('Job.count') do
      post :create, job: { career_level_id: @job.career_level_id, client_id: @job.client_id, contact_details: @job.contact_details, content: @job.content, education_level_id: @job.education_level_id, expire_date: @job.expire_date, job_subcategory_id: @job.job_subcategory_id, job_tittle: @job.job_tittle, job_type_id: @job.job_type_id, location: @job.location, salary_id: @job.salary_id, start_date: @job.start_date }
    end

    assert_redirected_to job_path(assigns(:job))
  end

  test "should show job" do
    get :show, id: @job
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @job
    assert_response :success
  end

  test "should update job" do
    patch :update, id: @job, job: { career_level_id: @job.career_level_id, client_id: @job.client_id, contact_details: @job.contact_details, content: @job.content, education_level_id: @job.education_level_id, expire_date: @job.expire_date, job_subcategory_id: @job.job_subcategory_id, job_tittle: @job.job_tittle, job_type_id: @job.job_type_id, location: @job.location, salary_id: @job.salary_id, start_date: @job.start_date }
    assert_redirected_to job_path(assigns(:job))
  end

  test "should destroy job" do
    assert_difference('Job.count', -1) do
      delete :destroy, id: @job
    end

    assert_redirected_to jobs_path
  end
end
