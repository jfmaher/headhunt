module SiteHelper

#Returns a string of the number of applies for a job that do not have a value for reject. If there are no values the string is empty.
  def newApplies (j)
    newApplies = j.applies.where({reject: nil}).length
    if newApplies == 0 then return '' else return '('+newApplies.to_s+')' end
  end
end
