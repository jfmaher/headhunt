module JobsHelper
  def format_location(j)
    if j.location == '' 
      return j.country.name 
    else 
      return j.location+', '+j.country.name 
    end 
  end  
end
