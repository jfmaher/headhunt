json.array!(@jobs) do |job|
  json.extract! job, :id, :job_title, :salary_id, :location, :job_type_id, :job_subcategory_id, :client_id, :start_date, :expire_date, :content, :education_level_id, :career_level_id, :contact_details
  json.url job_url(job, format: :json)
end
