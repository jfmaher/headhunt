json.array!(@candidates) do |candidate|
  json.extract! candidate, :id, :first_name, :last_name, :phone, :location, :user_id
  json.url candidate_url(candidate, format: :json)
end
