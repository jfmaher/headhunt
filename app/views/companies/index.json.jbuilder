json.array!(@companies) do |company|
  json.extract! company, :id, :name, :addressHQ, :phone, :logopath, :owner, :website, :supportemail, :googlemapURL
  json.url company_url(company, format: :json)
end
