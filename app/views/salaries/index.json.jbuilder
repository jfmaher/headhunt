json.array!(@salaries) do |salary|
  json.extract! salary, :id, :min, :max, :salary_type_id
  json.url salary_url(salary, format: :json)
end
