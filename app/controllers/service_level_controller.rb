class ServiceLevelController < ApplicationController
before_action :check_for_client

def levels
  @levels = ServiceLevel.all
end

def select
  current_client.service_level_id = selectParams[:service_level_id]
  current_client.save
  redirect_to client_root_path
end

protected
  def selectParams
    params.permit(:service_level_id)
  end

private
  def check_for_client
    redirect_to root_path unless client_signed_in?
  end
end
