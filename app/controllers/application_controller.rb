class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  private
  def after_sign_up_path_for(resource)
    new_user_session_path
   end
  
  def after_sign_out_path_for(resource_or_scope)
    site_main_path
  end
end
