class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]

  # GET /jobs
  # GET /jobs.json
  def index
    @search = Job.search(params[:q])
    @jobs = @search.result
    @jobs = @jobs.sort_by(&:created_at).reverse
	@categories = JobType.pluck(:name)
	@categories.sort!
    @selected = Hash.new
    @selected[:job_type] = params[:q].nil? ? nil : params[:q][:job_type_id_eq]
    @selected[:country] = params[:q].nil? ? nil : params[:q][:country_id_eq]
  end
  
  

  # GET /jobs/1
  # GET /jobs/1.json
  def show
    @companyName = @job.company.nil? ? 'No company' : @job.company.name 
  end

  # GET /jobs/new
  def new
    redirect_to root_path unless client_signed_in?
    @job = Job.new
  end

  # GET /jobs/1/edit
  def edit
  end

  # POST /jobs
  # POST /jobs.json
  def create
    if client_signed_in? && current_client.service_level.job_limit <= current_client.jobs.length 
      redirect_to root_path 
    else
      @job = Job.new(job_params)
      @job.client_id = current_client.id
      @job.save
      
      respond_to do |format|
        if @job.save
          format.html { redirect_to @job, notice: 'Job was successfully created.' }
          format.json { render action: 'show', status: :created, location: @job }
        else
          format.html { render action: 'new' }
          format.json { render json: @job.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    redirect_to root_path unless client_signed_in? && current_client.id == @job.client_id
    @job_id = @job.id
    @job.destroy
    respond_to do |format|
      format.html { redirect_to jobs_url }
      format.json { head :no_content }
      format.js   { render 'manager_destroy.js'}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:job_title, :salary_id, :location, :country_id, :job_type_id, :job_subcategory_id, :client_id, :start_date, :expire_date, :content, :education_level_id, :career_level_id, :contact_details)
    end
end
