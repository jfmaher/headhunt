class SiteController < ApplicationController
  def main
	@search = Job.search(params[:q])
  end


  def contact
  end


  def about
  end


  def cv
  end
  
  
  def recruiter
    redirect_to 'site/login' unless client_signed_in?
    if current_client.service_level.nil?
      @moreJobs = false
    else
      @moreJobs = current_client.service_level.job_limit > current_client.jobs.length
    end
    @example = current_client
    @jobs = Job.where({:client_id => current_client.id})
    @recentPosts = Job.where({client_id: current_client.id}).order(created_at: :desc).take(3)
    @serviceName = current_client.service_level.nil? ? "Free" : current_client.service_level.name
    @company = Company.find(current_client.company_id) unless current_client.company_id == nil
  end
  
  
  def recruiter_job_management
    @jobs = Job.where({:client_id => current_client.id})
  end

  def applications_manager
    @jobs = Job.where({:client_id => current_client.id})
  end
  
  def reg
    @client = Client.new
  end
  
  def candidate
    if candidate_signed_in?
      @saved = CandidateJob.where({candidate_id: current_candidate.id}).pluck(:job_id)
      @saved = Job.find(@saved)
      @applications = Apply.where({candidate_id: current_candidate.id, active: true})
    else
      redirect_to('/site/login')
    end
  end
  
  def candidate_search
    @search = Candidate.search(params[:q])
    @candidates = @search.result
  end
end
