class CandidateJobsController < ApplicationController
respond_to :js
  def new
    @cj = CandidateJob.new(create_params)
    @cj.candidate_id = current_candidate.id
    @jobId = create_params[:job_id]
    @cj.save
    @cjId = @cj.id
  end
  def destroy
    @cj = CandidateJob.find(params[:id])
    @jobId = @cj.job_id
    @cj.destroy
  end

protected
  def create_params
    params.permit(:job_id)
  end
end
