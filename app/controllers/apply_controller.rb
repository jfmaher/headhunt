class ApplyController < ApplicationController
respond_to :js
  def apply
    @application = Apply.where({:candidate_id => current_candidate.id, :job_id => jobId[:job_id]}).take
    if @application.nil? then @application = Apply.new({:candidate_id => current_candidate.id, :job_id => jobId[:job_id]}) end
    @application.active = true
    @application.save
  end

  def destroy
    @application = Apply.find(params[:id])
    @application.active = false
    @application.save
    #Job needed to create button again.
    @job = Job.find(@application.job_id)
  end

  def remove
    @job = Job.find(params[:job_id])
    @job.applies.each do |app|
      app.reject = true
      app.save
    end
    render nothing: true
  end
protected
  def jobId
    params.permit(:job_id)
  end
end
