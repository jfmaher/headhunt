// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require_tree .


//For applications manager on the recruiter profile page. Shows and removes the table that contains the applicatants from view. It also sends a ajax reques to mark all the applicants of the job as seen. 
function hide_table(id){
  var title = $('#applications-job-title-'+id);
  title.toggleClass('job-title-application').toggleClass('job-title-application-open');
  var titleStr = title.text();
  if (titleStr.charAt(titleStr.length-1) == ')'){
    title.children().first().text(titleStr.substr(0, titleStr.length-4));
  }
  var block = $('#table-container-'+id);
  block.css('display', (block.css('display') == 'none' ?'block':'none'));
  removeNew = new XMLHttpRequest();
  removeNew.open('GET','/remove?job_id='+id,true);
  removeNew.send();
}
