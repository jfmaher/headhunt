class CandidateJob < ActiveRecord::Base
	has_one :candidate
	has_one :job
    validates :candidate_id, presence: true 
    validates :job_id, presence: true 
end
