class Job < ActiveRecord::Base
belongs_to :client
belongs_to :subcategory
belongs_to :salary
belongs_to :job_type
belongs_to :country
has_one :company, through: :client
has_many :candidate_jobs
has_many :applies
validates :job_title, presence: true
validates :job_type_id, presence: true
validates :country_id, presence: true
end
