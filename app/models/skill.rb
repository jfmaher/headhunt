class Skill < ActiveRecord::Base
has_many :knowledges
has_many :candidates, :through => :knowledges

end
