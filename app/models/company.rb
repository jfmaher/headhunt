class Company < ActiveRecord::Base
  has_one :client, dependent: :nullify
  has_attached_file :logo
  validates_attachment :logo , :content_type => {:content_type => ['image/jpeg']}
end
