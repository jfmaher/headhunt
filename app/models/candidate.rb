class Candidate < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
has_many :knowledges
has_many :skills, :through => :knowledges
has_many :applies
has_many :candidate_jobs
has_many :jobs, :through => :applies
has_attached_file :cv 

validates_attachment :cv, :content_type => { :content_type => ['application/txt', 'text/plain',
'application/pdf', 'application/msword',
'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
'application/vnd.oasis.opendocument.text',
'application/x-vnd.oasis.opendocument.text',
'application/rtf', 'application/x-rtf', 'text/rtf', 
'text/richtext', 'application/doc', 'application/docx', 'application/x-soffice', 'application/octet-stream'] }

end

