class Apply < ActiveRecord::Base
  belongs_to :candidate
  belongs_to :job
  has_one :client, :through => :job
end
