class Client < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :position
  has_one :company
  belongs_to :service_level
  has_many :jobs
  has_many :applies, :through => :jobs
end
