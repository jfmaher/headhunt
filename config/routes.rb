Headhunt::Application.routes.draw do
  
  devise_for :clients, :controllers => {:registrations => "clients/registrations"}
#Different controller for the extra parameters
  devise_for :candidates, :controllers => {:registrations => "candidates/registrations"}

  resources :jobs

  resources :salaries

  resources :salary_types

  resources :job_types

  resources :clients

  resources :companies

  resources :positions

#The actions for ajax that saves jobs for users.
  delete "candidate_jobs", to: "candidate_jobs#destroy"
  post "candidate_jobs", to: "candidate_jobs#new"

#The actions for the ajax responces for applying to jobs
  delete "apply", to: "apply#destroy"
  post "apply", to: "apply#apply"
  get "remove", to: "apply#remove"

#The actions for the service level
  get "service_level/levels"
  post "service_level/select"

#Recruiter profile pages
  get "site/recruiter", as: 'client_root'
  get "site/recruiter_job_management"
  get "site/applications_manager"

#Candidate proile pages
  get "site/candidate", as: 'candidate_root'

#Joint login page
  get "site/login"

  get "site/contact"
  get "site/about"
  get "site/candidate_search"

#Home
  get "site/main"
  root to: "site#main"

  resources :user_types  
  
  resources :subcategories

  resources :categories

  resources :candidates do
    collection do
      patch 'add_cv'
      delete 'destroy_cv'
    end
  end

  resources :skills
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
