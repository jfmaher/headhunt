# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Headhunt::Application.config.secret_key_base = 'fac5219b07923c95793a4f48346ad6216b51e6f24bef1e59fd2f96cd2a0be85225ef29caf354bbcb4504ce61887cdfba895e6b9389db98ba509be7941e0aa293'
